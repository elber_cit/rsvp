<?php
/**
 * @file
 * @Contains \Drupal\rsvplists\EnablerService
 */

 namespace Drupal\rsvplist;

 use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\jsonapi\JsonApiResource\Data;
use Drupal\node\Entity\Node;
use Drupal\views\Plugin\views\row\Fields;

/**
  * Defines a service for managing RSVP List enabled for nodes
  */
  class EnablerService{
      /**
       * Constructor
       */
      public function __construct() {

      }
          /**
           * Sets a indiviudal node to be RSVP enabled.
           * 
           * @param \Drupal\node\Entity\Node $node
           */
          function setEnabled(Node $node) {
              if(!$this->isEnabled($node)) {
                  $insert = Database::getConnection()->insert('rsvplist_enabled');
                  $insert->fields(array('nid'), array($node->id()));
                  $insert->execute();
              }
          }
            /**
             * Checks if an individual node is RSVP enabled.
             * 
             * @param \Drupal\node\Entity\Node $node;
             * 
             * @return bool
             * Weather the node is enabled for the RSVP functionality
             */
           function isEnabled(Node $node){
               if($node->isNew()){
                   return false;
               }
               $select = Database::getConnection()->select('rsvplist_enabled', 're');
               $select->fields('re', array('nid'));
               $select->condition('nid', $node->id());
               $results = $select->execute();
               return !empty($results->fetchCol());
            }  
            /**
            * Deletes enabled settings for an individual nodes.
            *
            * @param \Drupal\node\Entify\Node $node
            */
            function delEnabled(Node $node){
                $delete = Database::getConnection()->delete('rsvplist_enabled');
                $delete->condition('nid', $node->id());
                $delete->execute();
            }
} 
         
