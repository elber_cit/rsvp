<?php
/**
 * @file
 * @Contains \Drupal\rsvplist\form\RSVPSettingsForm
 */

namespace Drupal\rsvplist\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form to configure RSVP List  module settings
 */
    /**
     * {@inheritdoc}
     */
    class RSVPSettingsForm extends ConfigFormBase{
        /**
         * {@inheritdoc}
         */
         public function getFormId()
         {
             return 'rsvplist_admin_settings';
         }
         /**
          * {@inheritdoc}
          */
         protected function getEditableConfigNames(){
             return [
                 'rsvplist.settings'  ];
         }
         /**
          * {@inheritdoc}
          */
 public function buildform(array $form, FormStateInterface $form_state, Request $request = null){
     $types = node_type_get_names();
     $config = $this->config('rsvplist.settings');
     $form ['rsvplist_types'] = array (
         '#type' => 'checkboxes',
         '#title' => $this-> t('The content types to enable RSVP collection for'),
         '#default_value' => $config->get('allowed types'),
         '#options' => $types,
         '#description' => t('On the specified node, an RSVP option will be availble and can enable while that node is being edited.'),
     );
     $form ['array_filter'] = array('#type' => 'value', '#value' =>true);
     return parent::buildform($form,$form_state);
    }
    
    public function submitForm(array &$form, FormStateInterface $form_state){
        $allowed_types = array_filter($form_state->getValue('rsvplist_types'));
        sort($allowed_types);
        $this->config('rsvplist.settings')
        ->set('allowed_types', $allowed_types)
        ->save();
        parent::submitForm($form, $form_state);
    }
}

 