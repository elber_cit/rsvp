<?php
/**
 * @file
 * Contains \Drupal\rsvplist\Form\Rsvpform
 */

 namespace Drupal\rsvplist\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Form;

/**
 * Provides an RSVP Email form.
 */
class RSVPForm extends FormBase{
/**
 * (@inheritdoc)
 */
public function getFormId(){
    return 'rsvplist_email_form';
}
public function buildForm(array $form, FormStateInterface $form_state){
    $node = \Drupal::routeMatch()->getParameter('node');
    if (isset($node)) {
        $nodeid = $node->id();
   }   
    $form['email'] = array(
        '#title' => t('Email Address'),
        '#type' => 'textfield',
        '#size' => 25,
        '#description' => t("We'll send update to the email address your provide."),
        '#required' => TRUE,
    );
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('RSVP'),
    );
    $form['nid'] = array(
        '#type' =>'hidden',
        '#value' => $nodeid,
    );
    return $form;
    }
    /**
     * (@inheritdoc)
     */
    public function validateForm(array &$form, FormStateInterface $form_state){
        $value = $form_state->getValue('email');
        if ($value ==!\Drupal::service('email.validator')->isvalid($value)){
            $form_state->setErrorByName('email', t('the email adress %mail is not valid.', array('%mail' => $value)));
        }
    }
    /**
     * (@inheritdoc)
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
        \Drupal::database()->insert('rsvplist')
        ->fields(array(
            'mail' => $form_state->getValue('email'),
            'nid' => $form_state->getValue('nid'),
            'uid' => $user->id(),
            'created' => time(),
        ))
        ->execute();
        \Drupal::messenger()->addMessage(t('Thank you for your RSVP, you are on the event list'));
    }
}